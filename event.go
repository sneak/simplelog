package simplelog

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID        uuid.UUID       `json:"id"`
	Timestamp time.Time       `json:"timestamp"`
	Level     string          `json:"level"`
	Message   string          `json:"message"`
	Data      json.RawMessage `json:"data"`
}

func NewEvent(level, message string, data json.RawMessage) Event {
	return Event{
		ID:        uuid.New(),
		Timestamp: time.Now().UTC(),
		Level:     level,
		Message:   message,
		Data:      data,
	}
}
