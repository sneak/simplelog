package main

import (
	"log/slog"

	_ "sneak.berlin/go/simplelog" // Using underscore to only invoke init()
)

func main() {
	// Send some test messages with structured data
	slog.Info("Starting the application", slog.String("status", "initialized"))
	slog.Info("Attempting to connect to database", slog.String("host", "localhost"), slog.Int("port", 5432))
	slog.Warn("Using default configuration", slog.String("configuration", "default"))
	slog.Error("Failed to load module", slog.String("module", "finance"), slog.String("error", "module not found"))
	slog.Info("Shutting down the application", slog.String("status", "stopped"))
}
