#!/bin/bash

# Set the environment variables for the RELP server URL and optionally for the webhook URL
export LOGGER_RELP_URL="tcp://10.201.1.18:20514"
#export LOGGER_WEBHOOK_URL="https://example.com/webhook"
export RELP_DEBUG=1

# Run the Go program
go run relp_log_trial.go
