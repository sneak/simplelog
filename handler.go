package simplelog

import "log/slog"

// Handler defines the interface for different log outputs.
type ExtendedHandler interface {
	slog.Handler
}
