.PHONY: test

default: test

test:
	@go test -v ./...

fmt:
	goimports -l -w .
	golangci-lint run --fix

lint:
	golangci-lint run
	sh -c 'test -z "$$(gofmt -l .)"'

docker:
	docker build --progress plain .
