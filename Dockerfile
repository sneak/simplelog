# First stage: Use the golangci-lint image to run the linter
FROM golangci/golangci-lint:latest as lint

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the go.mod file and the rest of the application code
COPY go.mod ./
COPY . .

# Run golangci-lint
RUN golangci-lint run

RUN sh -c 'test -z "$(gofmt -l .)"'

# Second stage: Use the official Golang image to run tests
FROM golang:1.22 as test

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the go.mod file and the rest of the application code
COPY go.mod ./
COPY . .

# Run tests
RUN go test -v ./...

# Final stage: Combine the linting and testing stages
FROM golang:1.22 as final

# Ensure that the linting stage succeeded
WORKDIR /app
COPY --from=lint /app .
COPY --from=test /app .

# Set the final CMD to something minimal since we only needed to verify lint and tests during build
CMD ["echo", "Build and tests passed successfully!"]

