# simplelog

## Summary

simplelog is an opinionated logging package designed to facilitate easy and
structured logging in Go applications with an absolute minimum of
boilerplate.

The idea is that you can add a single import line which replaces the
stdlib `log/slog` default handler, and solve the 90% case for logging.

## Current Status

Released v1.0.0 2024-06-14.  Works as intended.  No known bugs.

## Features

-   if output is a tty, outputs pretty color logs
-   if output is not a tty, outputs json
-   supports delivering each log message via a webhook

## Planned Features

-   supports delivering logs via tcp RELP (e.g. to remote rsyslog using imrelp)

## Installation

To use simplelog, first ensure your project is set up with Go modules:

```bash
go mod init your_project_name
```

Then, add SimpleLog to your project:

```bash
go get sneak.berlin/go/simplelog
```

## Usage

Below is an example of how to use SimpleLog in a Go application. This
example is provided in the form of a `main.go` file, which demonstrates
logging at various levels using structured logging syntax.

```go
package main

import (
	"log/slog"
	_ "sneak.berlin/go/simplelog"
)

func main() {

    // log structured data with slog as usual:
	slog.Info("User login attempt", slog.String("user", "JohnDoe"), slog.Int("attempt", 3))
	slog.Warn("Configuration mismatch", slog.String("expected", "config.json"), slog.String("found", "config.dev.json"))
	slog.Error("Failed to save data", slog.String("reason", "permission denied"))
}
```

## License

[WTFPL](./LICENSE)
