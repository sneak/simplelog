package main

import (
	"log/slog"

	_ "sneak.berlin/go/simplelog"
)

func main() {

	// log structured data with slog as usual:
	slog.Info(
		"User login attempt",
		slog.String("user", "JohnDoe"),
		slog.Int("attempt", 3),
	)
	slog.Warn(
		"Configuration mismatch",
		slog.String("expected", "config.json"),
		slog.String("found", "config.dev.json"),
	)
	slog.Error(
		"Failed to save data",
		slog.String("reason", "permission denied"),
	)
}
