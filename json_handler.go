package simplelog

import (
	"context"
	"encoding/json"
	"log"
	"log/slog"
)

type JSONHandler struct{}

func NewJSONHandler() *JSONHandler {
	return &JSONHandler{}
}

func (j *JSONHandler) Handle(ctx context.Context, record slog.Record) error {
	jsonData, _ := json.Marshal(record)
	log.Println(string(jsonData))
	return nil
}

func (j *JSONHandler) Enabled(ctx context.Context, level slog.Level) bool {
	return true
}

func (j *JSONHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return j
}

func (j *JSONHandler) WithGroup(name string) slog.Handler {
	return j
}
